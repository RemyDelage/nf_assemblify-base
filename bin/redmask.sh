#!/usr/bin/env bash
###############################################################################
##                                                                           ##
##               	Repeats masking regions by using RED                 ##
##                                                                           ##
###############################################################################

# var settings
args=("$@")
GENOME="${args[0]}"
NCPUS="${args[1]}"
LOGCMD="${args[2]}"

# Command to execute
CMD="Red -gnm . -msk masked -cor ${NCPUS}"

# Save command in log
echo ${CMD} > ${LOGCMD}

# Create result directory
mkdir -p masked

# Execute command
eval ${CMD}
