#!/usr/bin/env bash
###############################################################################
##                                                                           ##
##   Genome indexing, reads mapping and indexing with hisat2                 ##
##                                                                           ##
###############################################################################

# var settings
args=("$@")
R1="${args[0]}"
R2="${args[1]}"
RED_MASKED_FASTA="${args[2]}"
NCPUS="${args[3]}"
LOGCMD="${args[4]}"
INTRON=20000
OUT_FILE=$(basename ${R1%.fastq*}).sam


# Commands to execute
CMD="hisat2-build -p ${NCPUS} ${RED_MASKED_FASTA} ${RED_MASKED_FASTA%.*} ; \
	hisat2 -p ${NCPUS} --no-unal -q -x ${RED_MASKED_FASTA%.*} -1 ${R1} -2 ${R2} --max-intronlen ${INTRON} > ${OUT_FILE}"

# Save command in log
echo ${CMD} > ${LOGCMD}

# Execute command
eval ${CMD}
