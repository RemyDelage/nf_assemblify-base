#!/usr/bin/env bash
###############################################################################
##                                                                           ##
##         PacBio reads quality control with Nanoplot                        ##
##                                                                           ##
###############################################################################

# var settings
args=("$@")
PACBIO=${args[0]}
LOGCMD=${args[1]}

# Command to execute
CMD="NanoPlot --plots kde dot hex --N50 \
	      --title 'PacBio Hifi reads for $(basename ${PACBIO%.hifi})' \
	      --fastq ${PACBIO} -o ."

# Keep command in log
echo ${CMD} > ${LOGCMD}

# Execute command
eval ${CMD}

