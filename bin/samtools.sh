#!/usr/bin/env bash
###############################################################################
##                                                                           ##
##   Conversion SAM files to BAM and indexation with samtools                ##
##                                                                           ##
###############################################################################

# var settings
args=("$@")
SAM_FILE="${args[0]}"
NCPUS="${args[1]}"
LOGCMD="${args[2]}"

OUT_FILE=$(basename ${SAM_FILE%.*}).bam


# Commands to execute

## Convertion from SAM to BAM
CMD1="samtools sort -o ${OUT_FILE} -@ ${NCPUS} -m 2G -O bam -T tmp ${SAM_FILE}" 
## Indexation of the BAM files
CMD2="samtools index ${OUT_FILE}" 


# Save commands in log
echo ${CMD1} > ${LOGCMD}
echo ${CMD2} >> ${LOGCMD}

# Execute commands
eval ${CMD1}
eval ${CMD2}

