#!/usr/bin/env bash
###############################################################################
##                                                                           ##
##               		 BUSCO of genome assembly	             ##
##                                                                           ##
###############################################################################

# var settings
args=("$@")
NCPUS=${args[0]}
DB_PATH="${args[1]}"
DB_NAME="${args[2]}"
ASSEMBLY="${args[3]}"
LOGCMD="${args[4]}"

# Command to execute
RES_FILE=$(basename ${ASSEMBLY%.*}).busco
CMD="busco -c ${NCPUS} --offline -m genome -i ${ASSEMBLY} -o ${RES_FILE} -l ${DB_PATH}/${DB_NAME}"

# Keep command in log
echo ${CMD} > ${LOGCMD}

# Execute command
eval ${CMD}

