#  Assemblify-base pipeline

This is a project associated to this [training material](https://bioinfo.gitlab-pages.ifremer.fr/teaching/fair-gaa/practical-case/).

Following content should be adapted to turn this README as something FAIR to describe the pipeline. Work to be done by training attendees.

## Introduction

Assemblify-base is a pipeline which uses **Nextflow (version: 23.04.1)** (Di Tommaso et al., 2017 ; [Nextflow GitHub repository](https://github.com/nextflow-io/nextflow)) for the sequences alignment on a reference genome. These sequences can be trancriptomic sequences (RNA) or genomic sequences (DNA) and the reads to align can come from Illumina or PacBio-HiFi sequencer.

There are five main stages on this pipeline, that allows to realize the quality control of the reads directly from sequencing or downloaded from databases ; the reads assembly into contings ; the masking of repeated regions ; the mapping of the reads and the genome annotation.

The using of **Nextflow** allows the users to directly realize all the analysis, only thanks to a simple command line. All the differents tools creates results folders, named according to the pipeline stage (see *Pipeline sketch* section). The fact that the pipeline is automated also gives users high reproducibility of analyses.

## Biological datasets

The genome used for this analysis is that of the *Dunaliella primolecta*, a micro-algae species. Two kind of genomic datas are available for the genome assembly : RNA sequences, produced by an Illumina sequencer and DNA sequences from a PacBio HiFi sequencer.

The sequences are available in many databases : 
* Study accession number : PRJEB46283 [ENA (EMBL-EBI)](https://www.ebi.ac.uk/ena/browser/view/PRJEB46283); [BioProject (NCBI)](https://www.ncbi.nlm.nih.gov/bioproject/PRJEB46283/)
* Samples accessions numbers :
	* RNA PolyA : SAMEA7524530 [BioSamples (EMBL-EBI)](https://www.ebi.ac.uk/biosamples/samples/SAMEA7524530); [BioSample (NCBI)](https://www.ncbi.nlm.nih.gov/biosample/SAMEA7524530)
	* PacBio - HiFi : SAMEA8100059 [BioSamples (EMBL-EBI)](https://www.ebi.ac.uk/biosamples/samples/SAMEA8100059); [BioSample (NCBI)](https://www.ncbi.nlm.nih.gov/biosample/?term=SAMEA8100059)
* Run accessions numbers :
	* RNA PolyA : ERR6688549 [ENA (EMBL-EBI)](https://www.ebi.ac.uk/ena/browser/view/ERR6688549); [SRA (NCBI)](https://trace.ncbi.nlm.nih.gov/Traces/?view=run_browser&acc=ERR6688549&display=metadata)
	* PacBio - HiFi : ERR6939244 [ENA (EMBL-EBI)](https://www.ebi.ac.uk/ena/browser/view/ERR6939244); [SRA (NCBI)](https://trace.ncbi.nlm.nih.gov/Traces/?view=run_browser&acc=ERR6939244&display=metadata)

## Computing environment

This pipeline was developed and implemented thanks to the [On-Demand cluster](https://ondemand.cluster.france-bioinformatique.fr) with this computing environment :
* OS : Unix
* Partition : fast
* Number of CPUs : 2
* Amount of memory : 8G

## Requirements

Before starting the pipeline, users must :
* Install [Nextflow](https://www.nextflow.io/docs/latest/getstarted.html#installation) and [Singularity](https://docs.sylabs.io/guides/3.0/user-guide/)
* Clone the GitLab fork of *Nf_Assemblify-base* (IFB) :
	* Click on **Fork project** on GitLab web interface
	* Copy your own project link and create your own git repository thanks to the following command lines
```
mkdir assemblify-tmp
git clone <your_gitlab_fork_of_assembly-base_project>
``` 

## Pipeline sketch

```
.
|-- 01a_fastqc
|-- 01b_multiqc
|-- 01c_nanoplot
|-- 02a_hifiasm
|-- 02b_busco
|   `-- hifiasm.asm.bp.p_ctg.busco
|       |-- logs
|       `-- run_chlorophyta_odb10
|-- 03a_redmask
|   `-- masked
|-- 04a_hisat2
|-- 04b_samtools
|-- 04c_multiqc_hisat2
`-- logs
    |-- busco
    |-- fastqc
    |-- hifiasm
    |-- hisat2
    |-- multiqc
    |-- multiqc_hisat2
    |-- nanoplot
    |-- redmask
    `-- samtools

```

### Step 1 - Quality Control

#### 1.1 FastQC

The quality control for the reads from sequencers was realized by **FastQC (version : 0.11.9)** [GitHub repository](https://github.com/s-andrews/FastQC?tab=readme-ov-file). It produces html file with all the descriptions of the quality of the sequencing :
* Basic statistics
* Per base sequence quality
* Per sequence quality score
* Per base sequence content
* Per sequence GC content
* Per base N content
* Sequence Length Distribution
* Sequence Duplication Levels
* Overrepresented sequences
* Adapter Content

The command used was : `fastqc -t ${NCPUS} --noextract ${fastq} -o .` with the following parameters :
* -t : the number of threads used for running the program.
* --noextract : do not extract archive files generated after analysis
* `${fastq} ` : the path to the fastq files used for the analysis
* -o : the output directory (here, the dot "." means the results were produced in the current directory)

#### 1.2 MultiQC

The quality control is also realized by **MultiQC (version : 1.14)** (Ewels et al., 2016 ; [GitHub repository](https://github.com/MultiQC/MultiQC)) which includes FastQC results but also produces a lot of statistical results : 
* Sequence Counts
* Sequence Quality Histograms 
* Per Sequence Quality Scores 
* Per Base Sequence Content
* Per Sequence GC Content 
* Per Base N Content 
* Sequence Length Distribution
* Sequence Duplication Levels 
* Overrepresented sequences
* Adapter Content 
* Status Checks

The command used was : `multiqc .`

#### 1.3 Nanoplot

Plotting tool for long read sequencing data and alignments. NanoPlot creates:
*    a statistical summary
*    a number of plots
*    a html summary file
 (De Coster & Rademakers, 2023 ; [GitHub repository](https://github.com/wdecoster/NanoPlot)).

The version used for this pipeline is **1.32.1**

In our case, Nanoplot was used to realize a quality control of the reads produced by the PacBio sequencer.

The command line was the following :
`NanoPlot -t ${NCPUS} --plots kde dot hex --N50 --title "PacBio Hifi reads for $(basename ${fastq%.hifi*})" --fastq ${fastq} -o nano_${fastq%%.*}`

Parameters : 
* -t : the number of threads used for running the program
* --plots : the type of plots generated (here : kde = Kernel Density Estimate ; dot = dot plot and hex =  Hexagonal Bin Plot)
* --N50 : the computation of the N50 of the genome
* --title : the graph title
* -o : the name of the output files.

### Step 2 -  Contiging

#### 2.1 Hifiasm

**Hifiasm (version : 0.18.9)** is the tool used for genome assembly from HiFi reads produced by PacBio sequencers. It can perform telomere-to-telomere assembly (Cheng et al., 2021, 2022 ; [GitHub repository](https://github.com/chhylp123/hifiasm))

The command used in this pipeline is the following : 

`hifiasm -k 51 -o ${name} -t${NCPUS} ${hifi}` 

The arguments are : 
* -k : the number of k-mers used for the alignments
* -o : the name of the output files
* -t : the number of threads used for running the program.

The argument `${hifi}` specifies that alignment is performed on HiFi reads produced by the PacBio sequencer.

It produces a fasta file with the assembly sequences and an assembly graph at gfa format.

#### 2.2 BUSCO

The completeness of the genome assembly realized by Hifiasm were computed by using **BUSCO (version : 5.5.0)** (Simão et al., 2015; Waterhouse et al., 2018; Seppey, Manni and Zdobnov, 2019; Manni et al., 2021 ; [GitLab reposoitory](https://gitlab.com/ezlab/busco))

It produces a lot of results, stored in numerous directories and statistics datas in tsv format.

The command used was : 

`busco -c ${NCPUS} --offline -m genome -i ${fasta} -o $(basename ${fasta%.*})_${dbName} -l ${dbPath}/${dbName}`

Parameters : 
* -c : the number of threads used for running the program
* --offline : used data already downloaded
* -m : evaluation mode (here `genome` means the evaluation were realized on a complete genome)
* -i : the imput file to analyze
* -o : the output directory
* -l : the input directories (`${dbPath}`: the directory of the database ; `${dbName}` : the name of the database)

### Step 3 - Reapat masking

#### RED

The masking of the repeated regions of the genome were realized with **RED (version : 2018.09.10)** (John Palmer, 2018 ; [GitHub repository](https://github.com/nextgenusfs/redmask/tree/master?tab=readme-ov-file))

It generates a folder containing a fasta file with all the repeated sequences that have been masked.

The command used is the following : 

`Red -gnm . -msk masked -cor ${NCPUS}`

Parameters : 
* -gnm : the path of the studied genome
* -msk : the output folder name
* -cor : the number of threads used for running the program.

### Step 4 - Evidences

#### 4.1 Hisat2

The indexation of the genome and the mapping of the reads produced by the Illumina sequencer was realized thanks to the **Hisat2 (version : 2.2.1)** assembler (Kim et al., 2019 ; [GitHub repository](https://github.com/DaehwanKimLab/hisat2))

The indexation of the genome was allowed thanks to this command :

`hisat2-build -p ${cpus} ${fasta} ${fasta%.*}`

Parameters : 
* -p : the number of threads used for running the program
* `${fasta}` : the path of the imput genome fasta file
* `${fasta%.*}`: The output files that takes the name of fasta files as prefixe.

This part produces the indexed genome files in the ht2 format.

The mapping of the reads produced by the Illumina sequencer on the previously indexed genome were realized thank to this command : 

`hisat2 -p ${cpus} --no-unal -q -x ${fasta} -1 ${R1} -2 ${R2} --max-intronlen ${intron}`

Parameters : 
* -p : the number of threads used for running the program
* --no-unal : avoid to create output files for the unaligned reads
* -q : indicates that input files are in fastq format
* -x : indicates that we uses the genome previously indexed for the mapping
* --max-intronlen : the maximum intron size allowed during alignment.

It produces a sam file that contains all the aligned reads.

#### 4.2 Samtools

The sam files are verry difficults to read and to manipulate due to their large size (approx. 18.2Gb). For indexation of the reads, we had to use **Samtools (version : 1.19)**(Danecek et al., 2021 ; [GitHub repository](https://github.com/samtools/samtools)).

The fist step were to convert the sam file into bam file thanks to the following command : 

`samtools sort -o ${OUT_FILE} -@ ${NCPUS} -m 2G -O bam -T tmp ${SAM_FILE}`

Parameters : 
* -o : the name of the output file
* -@ : the number of threads used for running the program
* -m : the maximum memory used by the sorting process
* -O : specify the output file format (here bam)
* -T : the temporary prefix for temporary files created during sorting. Temporary files are used during the sorting process and are deleted after sorting.

It produce a bed file from the sam file.

The second step were the indexation of the aligned reads thanks to this command : 

`samtools index ${OUT_FILE}`

Parameters : 
* `${OUT_FILE}` : the bed file to be indexed prevoiously created.

It produces a bai format file that correspond to the indexed reads.

#### 4.3 MultiQC of Hisat2

After the genome mapping and indexation, we had to realize another quality control thanks to **MultiQC (version : 1.14)** (Ewels et al., 2016 ; [GitHub repository](https://github.com/MultiQC/MultiQC)).

The command used was the same as the one used in step 1.2 and the html file produces contains the same information as the MultiQC report generated on the early stages of the analysis.

## License

# License

Assemblify-base pipeline © 2024 by Remy Delage & Antoine Daussin is licensed under [Attribution-NonCommercial 4.0 International](https://creativecommons.org/licenses/by-nc/4.0/).

[![CC BY-NC 4.0 License](https://mirrors.creativecommons.org/presskit/buttons/88x31/png/by-nc.png)](https://creativecommons.org/licenses/by-nc/4.0/)


## Authors
* Remy Delage, M2 bioinformatics student, University of Rennes, France
* Antoine Daussin, M2 bioinformatic student, University of Rennes, France  

## References

* Cheng H., Concepcion G.T., Feng X., Zhang H. and Li H. (2021) _Haplotype-resolved de novo assembly using phased assembly graphs with hifiasm_ . Nat Methods, 18:170-175. https://doi.org/10.1038/s41592-020-01056-5

* Cheng H., Jarvis E.D., Fedrigo O., Koepfli K.P., Urban L., Gemmell N.J. and Li H. (2022) _Haplotype-resolved assembly of diploid genomes without parental data_ . Nature Biotechnology, 40:1332-1335. https://doi.org/10.1038/s41587-022-01261-x

* Danecek, Petr, James K Bonfield, Jennifer Liddle, John Marshall, Valeriu Ohan, Martin O Pollard, Andrew Whitwham, et al. (2021) _Twelve Years of SAMtools and BCFtools _. GigaScience 10 (2): giab008. https://doi.org/10.1093/gigascience/giab008.

* Di Tommaso, P. et al. (2017) _Nextflow enables reproducible computational workflows_, Nature Biotechnology, 35(4), pp. 316-319. Available at: https://doi.org/10.1038/nbt.3820.

* Ewels P., Magnusson M. ,Lundin S. and Källe . (2016) _MultiQC: summarize analysis results for multiple tools and samples in a single report_ . Bioinormatics. volume 32. 19:3047.  https://doi.org/10.1093/bioinformatics/btw354

* Manni, M. et al. (2021) _BUSCO update: novel and streamlined workflows along with broader and deeper phylogenetic coverage for scoring of eukaryotic, prokaryotic, and viral genomes_ . arXiv. Available at: http://arxiv.org/abs/2106.11799.

* Kim, Daehwan, Joseph M. Paggi, Chanhee Park, Christopher Bennett, and Steven L. Salzberg (2019) _Graph-Based Genome Alignment and Genotyping with HISAT2 and HISAT-Genotype_. Nature Biotechnology 37 (8): 907-15. https://doi.org/10.1038/s41587-019-0201-4.

* Seppey, M., Manni, M. and Zdobnov, E.M. (2019) _BUSCO: Assessing Genome Assembly and Annotation Completeness_ , in M. Kollmar (ed.) Gene Prediction. New York, NY: Springer New York (Methods in Molecular Biology), pp. 227-245. Available at: https://doi.org/10.1007/978-1-4939-9173-0_14

* Simão, F.A. et al. (2015) _BUSCO: assessing genome assembly and annotation completeness with single-copy orthologs_ , Bioinformatics, 31(19), pp. 3210-3212. Available at: https://doi.org/10.1093/bioinformatics/btv351.

* Waterhouse, R.M. et al. (2018) _BUSCO Applications from Quality Assessments to Gene Prediction and Phylogenomics_ , Molecular Biology and Evolution, 35(3), pp. 543-548. Available at: https://doi.org/10.1093/molbev/msx319.
