process multiqc {

    // label is used to find appropriate resources to use with this task.
    // see conf/resources.conf
    label 'lowmem'

    // tag is used to display task name in Nextflow logs
    tag "multiQC"

    // Result files published in Netxtflow result directory.
    // See also 'output' directive, below.
    publishDir "${params.resultdir}/01b_multiqc",	mode: 'copy', pattern: '*.html'
    publishDir "${params.resultdir}/logs/multiqc",	mode: 'copy', pattern: 'multiqc*.log'
    publishDir "${params.outdir}/00_pipeline_info/cmd",	mode: 'copy', pattern: 'multiqc*.cmd'

    // Workflow input stream. Since this stream can be a list of fastq files,
    // we use 'each()' to let Nextflow run this process on each file.
    input:
        path(qc_ch)

    // Workflow output stream
    output:
        path("*.html")
        path("multiqc*.log")
        path("multiqc*.cmd")

    // Script to execute (located in bin/ sub-directory)
    script:
    """
    multiqc.sh "." "multiqc_report" multiqc.cmd >& multiqc.log 2>&1
    """ 
}



