process hifiasm {

    // label is used to find appropriate resources to use with this task.
    // see conf/resources.conf
    label 'midmem'

    // tag is used to display task name in Nextflow logs
    tag "hifiasm"

    // Result files published in Netxtflow result directory.
    // See also 'output' directive, below.
    publishDir "${params.resultdir}/02a_hifiasm",	mode: 'copy', pattern: '*.bp.p_ctg.fa'
    publishDir "${params.resultdir}/logs/hifiasm",	mode: 'copy', pattern: 'hifi*.log'
    publishDir "${params.outdir}/00_pipeline_info/cmd",	mode: 'copy', pattern: 'hifi*.cmd'

    // Workflow input stream. Since this stream can be a list of fastq files,
    // we use 'each()' to let Nextflow run this process on each file.
    input:
        path(qc_ch)

    // Workflow output stream
    output:
        // results from this step will be available using 'assembly_fa' name
        path("*.bp.p_ctg.fa"), emit: assembly_fa
        // we also collect commands executed and log files
        path("hifi*.log")
        path("hifi*.cmd")


    // Script to execute (located in bin/ sub-directory)    
    script:
    """
    hifiasm.sh $qc_ch ${task.cpus} hifiasm.cmd >& hifiasm.log 2>&1
    """ 
}



