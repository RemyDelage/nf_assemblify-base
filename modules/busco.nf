process busco {

    // label is used to find appropriate resources to use with this task.
    // see conf/resources.conf
    label 'midmem'

    // tag is used to display task name in Nextflow logs
    // Here we use input channel file name
    tag "busco"

    // Result files published in Netxtflow result directory.
    // See also 'output' directive, below.
    publishDir "${params.resultdir}/02b_busco",		mode: 'copy', pattern: '*.busco'
    publishDir "${params.resultdir}/logs/busco",	mode: 'copy', pattern: '*.log'
    publishDir "${params.outdir}/00_pipeline_info/cmd",	mode: 'copy', pattern: '*.cmd'

    // Workflow input stream. Since this stream can be a list of fastq files,
    // we use 'each()' to let Nextflow run this process on each file.
    input:
	path(busco_bank_ch)
	val(busco_lineage_ch)
	path(assembly_fa)

    // Workflow output stream
    output:
        // results from this step will be available using 'na_ch' name, vu que c'est un sous dossier on met *.*
        path("*")

    // Script to execute (located in bin/ sub-directory)
    script:
    """
    busco.sh ${task.cpus} ${busco_bank_ch} ${busco_lineage_ch} ${assembly_fa} busco.cmd >& busco.log 2>&1
    """ 
}

