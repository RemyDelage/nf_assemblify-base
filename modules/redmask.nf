process redmask {

    // label is used to find appropriate resources
    label 'midmem'

    // tag is used to display task name in Nextflow logs
    tag "redmask"

    // Result files published in Netxtflow result directory.
    publishDir "${params.resultdir}/03a_redmask",       mode: 'copy', pattern: 'masked/*.msk'
    publishDir "${params.resultdir}/logs/redmask",      mode: 'copy', pattern: '*.log'
    publishDir "${params.outdir}/00_pipeline_info/cmd", mode: 'copy', pattern: '*.cmd'

    // Workflow input stream
    input:
        path(assembly_fa)

    // Workflow output stream
    output:
        path("masked/*.msk"), emit: redmask_ch
        path("*.log")
        path("*.cmd")

    // Script to execute (located in bin/ sub-directory)
    script:
    """
    redmask.sh ${assembly_fa} ${task.cpus} redmask.cmd >& redmask.log 2>&1
    """ 
}

