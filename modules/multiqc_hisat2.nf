process multiqc_hisat2 {

    // label is used to find appropriate resources to use with this task.
    // see conf/resources.conf
    label 'lowmem'

    // tag is used to display task name in Nextflow logs
    tag "multiqc_hisat2"

    // Result files published in Netxtflow result directory.
    // See also 'output' directive, below.
    publishDir "${params.resultdir}/04c_multiqc_hisat2",    mode: 'copy', pattern: '*.html'
    publishDir "${params.resultdir}/logs/multiqc_hisat2",    mode: 'copy', pattern: '*.log'
    publishDir "${params.outdir}/00_pipeline_info/cmd",    mode: 'copy', pattern: '*.cmd'

    // Workflow input stream. Since this stream can be a list of fastq files,
    // we use 'each()' to let Nextflow run this process on each file.
    input:
        path(hisat2_log_ch)

    // Workflow output stream
    output:
        path("*.html")
        path("*.log")
        path("*.cmd")

    // Script to execute (located in bin/ sub-directory)
    script:
    """
    multiqc.sh "." "multiqc_hisat2_report" multiqc_hisat2.cmd >& multiqc_hisat2.log 2>&1
    """ 
}
