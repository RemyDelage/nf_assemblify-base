process nanoplot {

    // label is used to find appropriate resources to use with this task.
    // see conf/resources.conf
    label 'lowmem'

    // tag is used to display task name in Nextflow logs
    // Here we use input channel file name
    tag "nanoplot_${na_ch}"

    // Result files published in Netxtflow result directory.
    // See also 'output' directive, below.
    publishDir "${params.resultdir}/01c_nanoplot",	mode: 'copy', pattern: '*.png'
    publishDir "${params.resultdir}/01c_nanoplot",	mode: 'copy', pattern: '*.txt'
    publishDir "${params.resultdir}/01c_nanoplot",	mode: 'copy', pattern: '*.html'
    publishDir "${params.resultdir}/logs/nanoplot",	mode: 'copy', pattern: '*.log'
    publishDir "${params.outdir}/00_pipeline_info/cmd",	mode: 'copy', pattern: '*.cmd'

    // Workflow input stream. Since this stream can be a list of fastq files,
    // we use 'each()' to let Nextflow run this process on each file.
    input:
        each(na_ch)

    // Workflow output stream
    output:
        // results from this step will be available using 'na_ch' name, vu que c'est un sous dossier on met *.*
        path("*.*")
        // we also collect commands executed and log files
        path("*.log")
        path("*.cmd")

    // Script to execute (located in bin/ sub-directory)
    script:
    """
    nanoplot.sh $na_ch nanoplot.cmd >& nanoplot.log 2>&1 
    """ 
}



