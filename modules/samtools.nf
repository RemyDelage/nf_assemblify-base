process samtools {

    // label is used to find appropriate resources
    label 'highmem'

    // tag is used to display task name in Nextflow logs
    tag "samtools"

    // Result files published in Netxtflow result directory.
    publishDir "${params.resultdir}/04b_samtools",      mode: 'copy', pattern: '*.bam'
    publishDir "${params.resultdir}/04b_samtools",      mode: 'copy', pattern: '*.bai'
    publishDir "${params.resultdir}/logs/samtools",     mode: 'copy', pattern: '*.log'
    publishDir "${params.outdir}/00_pipeline_info/cmd", mode: 'copy', pattern: '*.cmd'

    // Workflow input stream
    input:
	path(sam_ch)

    // Workflow output stream
    output:
	path("*.bam"), emit : bam_ch
	path("*.bai"), emit : bai_ch
	path("*.log")
	path("*.cmd")

    // Script to execute (located in bin/ sub-directory)
    script:
    """
    samtools.sh ${sam_ch} ${task.cpus} samtools.cmd >& samtools.log 2>&1
    """ 
}
