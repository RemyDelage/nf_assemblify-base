process hisat2 {

    // label is used to find appropriate resources
    label 'highmem'

    // tag is used to display task name in Nextflow logs
    tag "hisat2"

    // Result files published in Netxtflow result directory.
    publishDir "${params.resultdir}/04a_hisat2", 	mode: 'copy', pattern: '*.ht2'
    publishDir "${params.resultdir}/04a_hisat2",	mode: 'copy', pattern: '*.sam'
    publishDir "${params.resultdir}/logs/hisat2",	mode: 'copy', pattern: '*.log'
    publishDir "${params.outdir}/00_pipeline_info/cmd", mode: 'copy', pattern: '*.cmd'

    // Workflow input stream
    input:
	path(illumina_r1_ch)
	path(illumina_r2_ch)
	path(redmask_ch)

    // Workflow output stream
    output:
	path("*.ht2")
	path("*.sam"), emit : sam_ch
	path("*.log"), emit : hisat2_log_ch
	path("*.cmd")

    // Script to execute (located in bin/ sub-directory)
    script:
    """
    hisat2.sh ${illumina_r1_ch} ${illumina_r2_ch} ${redmask_ch} ${task.cpus} hisat2.cmd >& hisat2.log 2>&1
    """ 
}
